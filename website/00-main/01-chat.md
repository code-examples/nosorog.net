---
layout: page
title: Telegram чат
permalink: /chat/
---

# Telegram чат

<br/>

### JavaScript

https://t.me/jsdev_ru

<br/>

### Java

https://t.me/javadev_ru

<br/>

### Oracle DBA и PL/SQL

https://t.me/oracle_dba_ru
